var questions = [
    { 
        'question' : 'ile Hubert ma pytań?',
        'answers' : ['6', '16', '26', '73'],
        'correctAnswer' : '16'
        
    },
    {
        'question' : 'jak ma na imie monika brodka?',
        'answers' : ['monika', 'ania', 'janusz', 'kunegunda'],
        'correctAnswer' : 'janusz'
        
    },
    {
        'question' : 'która jest godzina?',
        'answers' : ['nie wiem', '9', '11', '16'],
        'correctAnswer' : '9'
        
    }
    
];

// console.log(questions[0].question);  // wyświetlenie pytania

var usedIndexes = [];
var currentQuestionNum = selectQuestionNum(usedIndexes);
var currentQuestion = questions[currentQuestionNum];


function selectQuestionNum()
{
    var q = Math.round(Math.random() * (questions.length-1));
    if (usedIndexes.length < questions.length)
    {
        while (usedIndexes.indexOf(q) >= 0)
        {
            q = Math.round(Math.random() * (questions.length-1));
        }
        usedIndexes.push(q);
    }
    else
    {
        q = -1;
    }
    return q
}

function fillData(question)
{
    var questionBox = document.getElementById('question');  // bo jeden tylko mamy element
    var answersElems = document.querySelectorAll('button');
    questionBox.innerHTML = question.question;
    
    for (var i=0; i<answersElems.length; i++)
    {
        answersElems[i].innerHTML = question.answers[i];
    }
}

function addListeners()
{
    var btns = document.querySelectorAll('button');
    
    for(var i=0; i<btns.length; i++)
    {
        btns[i].addEventListener('click', function(e)
        {
            if (e.target.innerHTML == currentQuestion.correctAnswer)  // target to element, na którym dokonano klikniecia
            {
                currentQuestionNum = selectQuestionNum();
                currentQuestion = questions[currentQuestionNum];
                fillData(currentQuestion);
            }
            else
            {
                alert('KONIEC GRY');
            }
        });
    }
    
}

addListeners();
fillData(currentQuestion);







