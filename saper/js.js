
var board;

var win;
var good = 0;

function init()
{
    document.getElementById("boardSize").value = 5;
    generateBoard(5, 5);
};

function flagClick(e)
{
    if(e.target.innerHTML.charCodeAt(0) > 48 && e.target.innerHTML.charCodeAt(0) < 58)
    {
        e.target.removeEventListener("contextmenu",flagClick);
    }
    
    e.target.innerHTML.split('');
        
    if(e.target.innerHTML == "F")
    {
        e.target.classList.remove("visitedFlag");
        e.target.innerHTML = "";
    }
    else
    {
        e.target.innerHTML = "F";
        e.target.classList.add("visitedFlag");
    }
    e.preventDefault();
}


function boardButtonClick(e)
{
    var id = e.target.id;
    var row = id.charAt(3);
    var col = id.charAt(5);
    
    var boardRow = board[row];
    var mine = boardRow[col];
    
    //console.log(typeof row);
    //console.log(row + ", " + col + ", mina: " + mine);
    
    if (mine===1)
    {
        e.target.classList.add("visitedMine");
        e.target.innerHTML = "B";
        //console.log(e.target.innerHTML);
        //console.log(document.getElementById("board"));
        setTimeout(function() 
        {
            var brd = document.getElementById("board");
            //console.log(brd.classList);
            brd.innerHTML = "przegrałeś! Spróbuj ponownie.";
        }, 500);
        
    }
    else
    {
 
        e.target.classList.add("visitedNoMine");
        // console.log(e.target);
        
        var counter = 0;
        
        
        
        var tab = ['bb_' + (parseInt(row)-1) + '_' + (parseInt(col)-1),
                   'bb_' + (parseInt(row)-1) + '_' + (parseInt(col)),
                   'bb_' + (parseInt(row)-1) + '_' + (parseInt(col)+1),
                   'bb_' + (parseInt(row)) + '_' + (parseInt(col)-1),
                   'bb_' + (parseInt(row)) + '_' + (parseInt(col)+1),
                   'bb_' + (parseInt(row)+1) + '_' + (parseInt(col)-1),
                   'bb_' + (parseInt(row)+1) + '_' + (parseInt(col)),
                   'bb_' + (parseInt(row)+1) + '_' + (parseInt(col)+1),
                  ];
        
        //console.log(tab);
        good = good+1;
        
        for(var i=0; i<tab.length; i++)
        {
            
            //console.log(document.getElementById(tab[i]));
            
            if(document.getElementById(tab[i]) != null)  
            {
                var sideId = tab[i];
                //console.log(sideId);
                var sideCol = (tab[i]).charAt(5);
                if(tab[i].charAt(5) == '_')
                {
                    sideCol = (tab[i]).charAt(6);
                }
                if(tab[i].charAt(6) == '-')
                {
                    sideCol = (tab[i]).charAt(7);
                }
                //console.log(sideCol);
                var sideRow = (tab[i]).charAt(3);
                if(tab[i].charAt(3) == '-')
                {
                    sideRow = (tab[i]).charAt(4);   
                }
                //console.log(sideRow);
                var boardRow = board[sideRow];
                //console.log(boardRow);
                var mine = boardRow[sideCol];
                //console.log(mine);
                
                if(mine === 1)
                {
                    counter++;
                }
            }
        
        }
        
        //console.log(counter);
        e.target.innerHTML = counter;
        e.target.classList.remove("visitedFlag");
        
        if(e.target.innerHTML.charCodeAt(0) > 47 && e.target.innerHTML.charCodeAt(0) < 58)
        {
            e.target.removeEventListener("contextmenu", flagClick);
            e.target.removeEventListener("click",boardButtonClick);
        }  
         
        
        if(e.target.innerHTML == '0')
        {
            var counterSide = 0;
            for(var i=0; i<tab.length; i++)
            {
                var temp = document.getElementById(tab[i]);
                
                if(temp != null)
                {
                    var x = temp.id.split('_');
                    
                    var mines = countMines(x[1], x[2]);
                    //console.log(tab[i]);
                    temp.classList.add("visitedNoMine");
                    console.log(temp.innerHTML);
                    if(temp.innerHTML == '')
                    {
                        good = good + 1;                       
                    }
                    
                    console.log('mines ' + mines);
                    temp.innerHTML = mines;
                
                }
            }
        }
        
        
        console.log(good);
        console.log(win);
        
        if (good == win)
        {
             setTimeout(function() 
            {
                var brd = document.getElementById("board");
                brd.innerHTML = "Wygrałeś! Zagraj jeszcze raz!";
            }, 200); 
        }
    }
    
}

function bbId(row, col)
{
    return "bb_" + row + "_" + col;
}

function generateBoard(rows, cols)
{
    var code = "<table>";
    
    board = [];
    
    for (var r = 0; r<rows; r++){
        code += "<tr>";
        for (var c=0; c<cols; c++)
        {
            var id = bbId(r,c);
            code += "<td><div class=\"boardButton\" id=\"" + id + "\"></div></td>";
        }
        code += "</tr>";
    }
    
    code += "</table>";
    document.getElementById("board").innerHTML = code;
    
        
    
    
    for (var r =0; r<rows; r++)
    {
        var boardRow = [];
        for (var c=0; c<cols; c++)
        {
            var id = bbId(r, c);
            var bb = document.getElementById(id);
            bb.addEventListener("click", boardButtonClick);
            bb.addEventListener("contextmenu", flagClick);
            
            
            
            var rnd = Math.random();
            if (rnd > 0.7)
            {
                boardRow[c] = 1;
            }
            else
            {
                boardRow[c] = 0;
            }
                
        }
        board[r] = boardRow;
        
    }
    
    sum = 0;
    for(var i=0; i<board.length; i++)
    {
        for(var j=0; j<board[i].length; j++)
        {
            sum += board[i][j];
        }
    }
    //console.log(sum);
    //console.log(board.length * board.length);
    var noBomb = board.length * board.length - sum;
    //console.log(noBomb);
    
    win = noBomb;
    console.log(win);
};    
    

function generateNewBoard()
{
    var size = document.getElementById("boardSize").value;
    good = 0;
    generateBoard(size, size);

};

function countMines(x, y) {
    var tab = [ [parseInt(x)-1, parseInt(y)-1],
                [parseInt(x)-1, parseInt(y)],
                [parseInt(x)-1, parseInt(y)+1],
                [parseInt(x), parseInt(y)-1],
                [parseInt(x), parseInt(y)+1],
                [parseInt(x)+1, parseInt(y)-1],
                [parseInt(x)+1, parseInt(y)],
                [parseInt(x)+1, parseInt(y)+1]
            ];
    
    var counter = 0;
    for(var i in tab) 
    {
        var temp = document.getElementById('bb_' + tab[i][0] + '_' + tab[i][1]);
        console.log("\tSprawdzam: bb_" + x + '_' + y);
        if(temp != null) 
        {
            if(board[tab[i][0]][tab[i][1]] == 1) 
            {
                counter++;
                console.log("\t\t", x, y, "jest bomba ", counter);
                
            }
        }
    }
    return counter;
}